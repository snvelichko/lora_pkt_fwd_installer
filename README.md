# LoRaWAN gateway packet forwarder installer

The [packet forwarder](https://github.com/Lora-net/packet_forwarder) is a program running on the host of a Lora gateway that forwards RF packets receive by the concentrator to a server through a IP/UDP link, and emits RF packets that are sent by the server. It can also emit a network-wide GPS-synchronous beacon signal used for coordinating all nodes of the network.

## Install packet forwarder

### Prepare project

Fetch the repository and restore the git submodules.

```shell
git clone https://gitlab.com/snvelichko/lora_pkt_fwd_installer.git
cd lora_pkt_fwd_installer
git submodule init
git submodule update
```

### Build and install service

Build the project and install.

```shell
make
sudo make install
```

### Uninstall service

Use the following command to uninstall the service.

```shell
sudo make uninstall
```

After removing the service, delete the configs.

```shell
sudo rm -R /opt/lora-net/
```

## Configure packet forwarder

Copy the global config and edit.

```shell
sudo cp /opt/lora-net/global_conf.json.dist /opt/lora-net/global_conf.json
sudo nano /opt/lora-net/global_conf.json
```

Copy the local config and edit.

```shell
sudo cp /opt/lora-net/local_conf.json.dist /opt/lora-net/local_conf.json
sudo nano /opt/lora-net/local_conf.json
```

## Usage packet forwarder service

Restart the systemd daemon after changing the startup script.

```shell
sudo systemctl daemon-reload
```

Activate the lora_pkt_fwd service.

```shell
sudo systemctl enable lora_pkt_fwd.service
sudo systemctl start lora_pkt_fwd.service
```

To view the status of the service, use the following command.

```shell
sudo systemctl status lora_pkt_fwd.service
```

To view the log, use the following command.

```shell
journalctl -u lora_pkt_fwd.service
```

To view the log in real time, use the following command.

```shell
journalctl -f -u lora_pkt_fwd.service
```