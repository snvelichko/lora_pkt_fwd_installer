### Environment constants 

INSTALL_PATH ?= /opt/lora-net
LORA_USER ?= lora
LORA_GROUP ?= lora
LORA_ADDGROUP ?= root
ARCH ?=
CROSS_COMPILE ?=
export

### general build targets
$(INSTALL_PATH)/lora_pkt_fw $(INSTALL_PATH)/global_conf.json $(INSTALL_PATH)/local_conf.json:
	$(MAKE) all -e -C lora_gateway 
	$(MAKE) all -e -C packet_forwarder

all: $(INSTALL_PATH)/lora_pkt_fw $(INSTALL_PATH)/global_conf.json $(INSTALL_PATH)/local_conf.json

install: $(INSTALL_PATH)/lora_pkt_fw $(INSTALL_PATH)/global_conf.json $(INSTALL_PATH)/local_conf.json
	### check root
	whoami | grep root || (echo "Please run install under root"; exit 1)

	### make program dir
	mkdir -p $(INSTALL_PATH)
	
	### copy program
	cp -f lora_gateway/reset_lgw.sh $(INSTALL_PATH)/reset_lgw.sh
	chmod +x $(INSTALL_PATH)/reset_lgw.sh
	cp -f packet_forwarder/lora_pkt_fwd/lora_pkt_fwd $(INSTALL_PATH)/lora_pkt_fwd
	
	### copy configs
	cp -f packet_forwarder/lora_pkt_fwd/global_conf.json $(INSTALL_PATH)/global_conf.json.dist
	cp -f packet_forwarder/lora_pkt_fwd/local_conf.json $(INSTALL_PATH)/local_conf.json.dist

	### copy startup script
	cp -f lora_pkt_fwd.service /etc/systemd/system/lora_pkt_fwd.service
	systemctl daemon-reload

	### create user and group
	getent group | grep "$(LORA_GROUP):" || groupadd -r $(LORA_GROUP)
	getent passwd | grep "$(LORA_USER):" || useradd -d $(INSTALL_PATH) -g $(LORA_GROUP) -G $(LORA_ADDGROUP) -M -r -s /bin/false $(LORA_USER)

	###
	###
	### Please change config and usage:
	###    sudo systemctl enable lora_pkt_fwd.service   - Enable service
	###    sudo systemctl disable lora_pkt_fwd.service  - Disable service
	###    sudo systemctl start lora_pkt_fwd.service    - Start service
	###    sudo systemctl stop lora_pkt_fwd.service     - Stop service
	###    sudo systemctl restart lora_pkt_fwd.service  - Restart service
	###    sudo systemctl status lora_pkt_fwd.service   - Status service
	###    sudo journalctl $(INSTALL_PATH)/lora_pkt_fwd - Show log
	###
	###

uninstall:
	### check root
	whoami | grep root || (echo "Please run uninstall under root"; exit 1)

	### remove service
	systemctl stop lora_pkt_fwd.service || echo "Service not loaded"
	systemctl disable lora_pkt_fwd.service || echo "Service not enabled"
	rm -f /etc/systemd/system/lora_pkt_fwd.service || echo "Service not installed"
	systemctl daemon-reload

	### remove configs
	rm -f $(INSTALL_PATH)/global_conf.json.dist || echo "Global config not found"
	rm -f $(INSTALL_PATH)/local_conf.json.dist || echo "Local config not found"

	### remove user and group
	getent passwd | grep "$(LORA_USER):" && userdel $(LORA_USER) || echo "User not found"
	getent group | grep "$(LORA_GROUP):" && groupdel $(LORA_GROUP) || echo "Group not found"

	### remove program
	rm -f $(INSTALL_PATH)/reset_lgw.sh
	rm -f $(INSTALL_PATH)/lora_pkt_fwd || echo "Program not installed"

	###
	###
	### Please manual run 'sudo rm -R $(INSTALL_PATH)' for remove config
	###
	###

clean:
	$(MAKE) clean -e -C lora_gateway
	$(MAKE) clean -e -C packet_forwarder

### EOF